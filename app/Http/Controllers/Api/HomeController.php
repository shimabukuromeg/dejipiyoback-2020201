<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return response()->json(['body' => 'でじぴよ', 'time' => Carbon::now()]);
    }
}
