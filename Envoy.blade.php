@servers(['ec2' => 'ec2-user@18.178.208.70'])

@setup
$appDir = '/app'
@endsetup

@story('deploy', ['on' => 'ec2'])
task-setup
task-deploy
@endstory

@task('task-setup')
cd {{ $appDir }}
git pull origin master
composer install --no-dev
@endtask

@task('task-deploy')
cd {{ $appDir }}
php artisan down
echo "DB使ってたらここで migrate とかする"
php artisan up
@endtask
